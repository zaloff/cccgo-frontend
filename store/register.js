export const state = () => ({
    nuevoUsuario: {},
    tempPassword: '',
    registerPlan: '',
    usuarioExterno: {},
    freeUserFlag: false,
    tempNewFreeId: 0
})

export const mutations = {
    SET_NUEVO_USUARIO(state, payload) {
        state.nuevoUsuario = payload
    },
    SET_TEMP_PASSWORD(state, payload) {
        state.tempPassword = payload
    },
    SET_REGISTER_PLAN(state, payload) {
        state.registerPlan = payload
    },
    SET_FREE_USER_FLAG(state, payload) {
        state.freeUserFlag = payload
    },
    SET_USUARIO_EXTERNO(state, payload) {
        state.usuarioExterno = payload
    },
    SET_TEMP_NEW_FREE_ID(state, payload) {
        state.tempNewFreeId = payload
    }
}

export const actions = {
    async saveNewUser({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`https://goccc-1d61d.firebaseio.com/users/CCC${payload.usuario}.json`, payload).then((res) => {
                this.$axios.patch(`https://goccc-1d61d.firebaseio.com/users/CCC${payload.usuario}.json`, {
                    contador_cancelacion: 0,
                    created_at: new Date()
                })
                commit('SET_NUEVO_USUARIO', payload)
                resolve('success')
            }).catch((error) => {
                reject(error)
            })
        })
    },
    async saveNewFreeUser({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`https://goccc-1d61d.firebaseio.com/free_users/${payload.usuario}.json`, payload).then((res) => {
                this.$axios.patch(`https://goccc-1d61d.firebaseio.com/free_users/${payload.usuario}.json`, {
                    contador_cancelacion: 0,
                    created_at: new Date()
                })
                commit('SET_NUEVO_USUARIO', payload)
                resolve('success')
            }).catch((error) => {
                reject(error)
            })
        })
    },
    async notifyCallCenter({ }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`${process.env.baseURL}/notifyCallCenter.php`, payload).then((res) => {
                resolve(res)
            }).catch((error) => {
                reject(error)
            })
        })
    },
    async checkPhonePontis({ }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`${process.env.baseURL}/check_mobile_pontis.php`, {
                phone: payload
            }).then((res) => {
                console.log('check pontis success:', res.data)
                resolve(res.data);
            }).catch((error) => {
                console.log('check pontis error:', error)
                reject(error);
            })
        })
    }
}

export const getters = {
    nuevoUsuario: (state) => {
        return state.nuevoUsuario
    },
    tempPassword: (state) => {
        return state.tempPassword
    },
    registerPlan: (state) => {
        return state.registerPlan
    },
    usuarioExterno: (state) => {
        return state.usuarioExterno
    },
    freeUserFlag: (state) => {
        return state.freeUserFlag
    },
    tempNewFreeId: (state) => {
        return state.tempNewFreeId
    }
} 