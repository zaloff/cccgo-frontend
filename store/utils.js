export const state = () => ({
  currentDate: '',
  expireDateTemp: ''
})

export const mutations = {
  SET_CURRENT_DATE(state, payload) {
    state.currentDate = payload
    /* state.currentDate = '24/02/2021' */
  },
  SET_EXPIRE_DATE_TEMP(state, payload) {
    state.expireDateTemp = payload
  }
}

export const actions = {
  async getCurrentDate({ commit }) {
    await this.$axios.get('/get_current_date.php').then((res) => {
      const formattedResponse = res.data.replace(/-/g, "/")
      commit('SET_CURRENT_DATE', formattedResponse)
    }).catch((error) => {
      console.log(error)
    })
  }
}

export const getters = {
  todayDate: (state) => {
    return state.currentDate
  },
  expireDateTemp: (state) => {
    return state.expireDateTemp
  }
}