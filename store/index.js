import Vue from 'vue'

export const state = () => ({
    snackbar: {
      x: 'right',
      y: 'top',
      state: false,
      color: 'success',
      timeout: 5000,
      message: ''
    }
})