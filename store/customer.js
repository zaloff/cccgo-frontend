export const state = () => ({
    devices: [],
    changeFlag: false,
    freeUserConfirmed: false
})

export const mutations = {
    SET_CUSTOMER_DEVICES(state, payload) {
        state.devices = payload
    },
    SET_CHANGE_FLAG(state, payload) {
        state.changeFlag = payload
    },
    SET_FREE_USER_CONFIRMED(state, payload) {
        console.log('SET_FREE_USER_CONFIRMED', payload)
        state.freeUserConfirmed = payload
    }
}

export const actions = {
    async getDevices({ commit }, payload) {
        await this.$axios.get(`/devices.php?id=${payload}`)
            .then((res) => {
                commit('SET_CUSTOMER_DEVICES', res.data.response.deviceDTOList)
            })
            .catch((err) => {
                console.log('error en store', err)
            })
    },
    async cancelSubscription({ }, payload) {
        return new Promise(async (resolve, reject) => {
            await this.$axios.post(`${process.env.baseURL}/set_status.php`, payload).then(async (res) => {
                await this.$axios.get(`https://goccc-1d61d.firebaseio.com/users/${payload.id}/contador_cancelacion.json`).then((result) => {
                    const contador = result.data + 1
                    this.$axios.patch(`https://goccc-1d61d.firebaseio.com/users/${payload.id}.json`, { contador_cancelacion: contador }).then((response) => {
                        console.log('update firebase =>', response)
                    }).catch((error) => {
                        console.log(error)
                    })
                })
                console.log('Suscripcion cancelada =>', res)
                resolve('ok')
            }).catch((error) => {
                console.log('error =>', error)
                reject(error)
            })
        })
    },
    isFreeUser({}, id) {
        return new Promise(async (resolve, reject) => {
            await this.$axios.get(`https://goccc-1d61d.firebaseio.com/free_users/${id}.json`)
                .then((res) => {
                    console.log('isFreeUser??:', res)
                    if (res.data == null) {
                        resolve('not free')
                    } else {
                        resolve('free')
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    confirmSuscription({}, id) {
        return new Promise(async (resolve, reject) => {
            await this.$axios.post(`${process.env.baseURL}/set_expire_date_pontis.php`, {
                date: null,
                id: id
            }).then(async (res) => {
                await this.$axios.get(`https://goccc-1d61d.firebaseio.com/free_users/${id}.json`)
                    .then(async (r) => {
                        await this.$axios.put(`https://goccc-1d61d.firebaseio.com/users/${id}.json`, r.data)
                            .then(async (response) => {
                                await this.$axios.delete(`https://goccc-1d61d.firebaseio.com/free_users/${id}.json`)
                                    .then((resp) => {
                                        resolve('confirmed')
                                    })
                                    .catch((er) => {
                                        reject(er)
                                    })
                            })
                            .catch((err) => {
                                reject(err)
                            })
                    })
                    .catch((e) => {
                        reject(e)
                    })
            }).catch((error) => {
                reject(error)
            })
        })
    },
    async isActiveUser({}, userID) {
        return new Promise(async (resolve, reject) => {
            const data = {
                usuario: userID.slice(3, userID.length)
            }
            await this.$axios.post(`${process.env.baseURL}/bs/isCCCActiveUser.php`, data).then((res) => {
                resolve(res.data.result)
            }).catch((error) => {
                reject(error)
            })
        })
    },
    async setWorkPhone({ }, payload) {
        const data = {
            user: payload.userID,
            value: payload.value
        }
        await this.$axios.post(`${process.env.baseURL}/set_customer_work_phone.php`, data).then((res) => {
            console.log(res)
        }).catch((error) => {
            console.log(error)
        })
    },
    deleteDevice({ commit }, payload) {
        commit('SET_CUSTOMER_DEVICES', payload)
    }

}

export const getters = {
    devices: (state) => {
        return state.devices
    },
    changeFlag: (state) => {
        return state.changeFlag
    },
    freeUserConfirmed: (state) => {
        return state.freeUserConfirmed
    }
}
