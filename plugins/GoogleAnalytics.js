import Vue from 'vue';
import VueGtag from 'vue-gtag';

Vue.use(VueGtag, {
    config: { id: 'G-TX03TF6B4Y' },
    appName: 'CCCGO',
});